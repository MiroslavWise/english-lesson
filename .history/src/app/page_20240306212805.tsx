import Link from "next/link";

import { LinksMainPage } from "./shared/main-page/Links";

import styles from "./page.module.css";
import { Head } from "./shared/main-page/head";

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.description}>
        <Head />
      </div>

      <div className={styles.center}>
        <span>
          Belarus is a landlocked country in Eastern Europe, bordered by Russia,
          Ukraine, Poland, Lithuania and Latvia. It is a place where history,
          culture and nature blend together in a unique way. Belarus has a rich
          and diverse heritage, from its medieval castles and churches to its
          soviet monuments and museums. It also boasts some of the most pristine
          and biodiverse natural areas in Europe, such as the Belavezhskaya
          Pushcha National Park, home to the largest population of European
          bison. Belarus is a destination for travelers who want to explore a
          lesser-known and authentic side of Europe, and enjoy its hospitality,
          cuisine and traditions.
        </span>
      </div>
      <LinksMainPage />
    </main>
  );
}
