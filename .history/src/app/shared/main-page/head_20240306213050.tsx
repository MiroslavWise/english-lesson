"use client";

import { motion } from "framer-motion";

export function Head() {
  return (
    <>
      <motion.p
        initial={{ y: 100, opacity: 0 }}
        animate={{ y: 0, opacity: 1 }}
        transition={{ ease: "easeInOut", duration: 1.5 }}>
        Start learning English from your native Belarus
      </motion.p>
      <motion.div>
        <a href='#' target='_blank' rel='noopener noreferrer'>
          By <h2>Avenir</h2>
        </a>
      </motion.div>
    </>
  );
}
