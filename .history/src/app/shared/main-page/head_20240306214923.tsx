"use client";

import { motion } from "framer-motion";

export function Head() {
  return (
    <>
      <motion.p
        initial={{ y: -100, x: -100, opacity: 0.3 }}
        animate={{ y: 0, x: 0, opacity: 1 }}
        transition={{ ease: "easeInOut", duration: 1.5 }}>
        Start learning English from your native Belarus
      </motion.p>
      <motion.div
        initial={{ y: -100, x: 100, opacity: 0.3 }}
        animate={{ y: 0, x: 0, opacity: 1 }}
        transition={{ ease: "easeInOut", duration: 1.5 }}>
        <a
          href='https://www.instagram.com/a.v.e.n.i.r_1705?igsh=MXBiMjRhZ3NycmU1cQ=='
          target='_blank'
          rel='noopener noreferrer'>
          By <h2>Avenir</h2>
        </a>
      </motion.div>
    </>
  );
}
