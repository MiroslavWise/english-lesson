"use client";

import { motion } from "framer-motion";

export function Head() {
  return (
    <>
      <motion.p>Start learning English from your native Belarus</motion.p>
      <motion.div>
        <a href='#' target='_blank' rel='noopener noreferrer'>
          By <h2>Avenir</h2>
        </a>
      </motion.div>
    </>
  );
}
