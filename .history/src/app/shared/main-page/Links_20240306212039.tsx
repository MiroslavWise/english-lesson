"use client";

import { motion } from "framer-motion";

export function LinksMainPage() {
  return (
    <>
      <div className='data-grid'>
        <motion.a
          initial={{ x: -200, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ ease: "easeInOut", duration: 2 }}
          href='https://www.britannica.com/place/Belarus'
          className='data-card first'
          target='_blank'
          rel='noopener noreferrer'>
          <h2>
            Belarus <span>-&gt;</span>
          </h2>
          <p>Belarus, landlocked country of eastern Europe</p>
        </motion.a>

        <motion.a
          initial={{ y: 100, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ ease: "easeInOut", duration: 2 }}
          href='https://president.gov.by/ru/gosudarstvo/simvolika/gimn'
          className='data-card anthem'
          target='_blank'
          rel='noopener noreferrer'>
          <h2>
            Anthem <span>-&gt;</span>
          </h2>
          <p>
            he national anthem has its own authors - the music was written by
            Belarusian composer N.F.Sokolovsky
          </p>
        </motion.a>

        <motion.a
          initial={{ y: 100, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ ease: "easeInOut", duration: 2 }}
          href='https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app'
          className='data-card'
          target='_blank'
          rel='noopener noreferrer'>
          <h2>
            Templates <span>-&gt;</span>
          </h2>
          <p>Explore starter templates for Next.js.</p>
        </motion.a>

        <motion.a
          initial={{ x: 200, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ ease: "easeInOut", duration: 2 }}
          href='https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app'
          className='data-card'
          target='_blank'
          rel='noopener noreferrer'>
          <h2>
            Deploy <span>-&gt;</span>
          </h2>
          <p>
            Instantly deploy your Next.js site to a shareable URL with Vercel.
          </p>
        </motion.a>
      </div>
    </>
  );
}
