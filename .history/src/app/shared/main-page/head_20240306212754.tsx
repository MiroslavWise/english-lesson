export function Head() {
  return (
    <>
      <p>Start learning English from your native Belarus</p>
      <div>
        <a href='#' target='_blank' rel='noopener noreferrer'>
          By <h2>Avenir</h2>
        </a>
      </div>
    </>
  );
}
